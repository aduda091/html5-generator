//funkcija za označavanje teksta, prima ID kontejnera kojeg
//želimo označiti
function selectText(containerid) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(containerid));
        range.select();
    } else if (window.getSelection()) {
        var range = document.createRange();
        range.selectNode(document.getElementById(containerid));
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
    }
}

$( document ).ready(function() {
    $("#preview").hide();
    $("#quickPreview").click(function() {
        $("#preview").slideToggle();
    })

});
