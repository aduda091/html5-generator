<?php 
if (isset($_POST['preview'])) {
    $lang = $_POST['lang'];
    $title = $_POST['title'];
    $header = $_POST['header'];
    $body = $_POST['body'];
    $footer = $_POST['footer'];


    $output = "<!DOCTYPE html>\n";
    $output.= "<html lang=\"{$lang}\">\n";
    $output.= "<head>\n";
    $output.= "\t<meta charset=\"UTF-8\">\n";
    
    $output.="\t<title>$title</title>\n";

    $output.="</head>\n<body>\n<header>\n";
    
    $output.="\t<h1>$header</h1>\n</header>\n<main>\n";

   
    $output.="\t<p>$body</p>\n</main>\n<footer>\n";

    
    $output.="\t<p>$footer</p>\n</footer>\n";

    $output.="</body>\n</html>";

    echo $output;
}




 ?>