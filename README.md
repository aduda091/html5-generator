# Duda's HTML5 generator #
Web aplikacija rađena u PHP programskom jeziku koja 
generira "boilerplate" HTML koda, a korisnik samo treba
unijeti naslov i osnovni tekst.

## TODO: ##
* RWD, 
* 1-click označavanje generiranog koda,
* 1-click kopiranje koda u clipboard,
* preview,
* dugmići za dodavanje extra tagova po volji za svaki element.