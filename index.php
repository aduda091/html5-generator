<!DOCTYPE html>
<html lang="hr">
<head>
    <meta charset="UTF-8">
    <title>HTML5 Generator</title>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?skin=desert"></script>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/script.js"></script>
</head>
<body>

<header>
    <h1>HTML5 Generator</h1>
    <p>Dobrodošli na generator HTML5 koda web stranica. Unesite željene podatke te kliknite gumb <strong>Generiraj</strong> kako bi vidjeli rezultate.</p>
</header>
<main>
<?php
if (isset($_POST['submit'])) {
    $lang = $_POST['lang'];
    $title = $_POST['title'];
    $header = $_POST['header'];
    $body = $_POST['body'];
    $footer = $_POST['footer'];
}


?>
    <form action="#" method="post" id="html-form">
        <label>Naslov:<input type="text" name="title" placeholder="Naslov Web stranice" required <?php if (isset($title)) echo "value=\"$title\"" ?> /></label>
        <label>Jezik:
            <select name="lang">
                <option value="hr" <?php if ($lang=="hr") echo "selected" ?>>Hrvatski</option>
                <option value="en" <?php if ($lang=="en") echo "selected" ?>>Engleski</option>
                <option value="de" <?php if ($lang=="de") echo "selected" ?>>Njemački</option>
                <option value="it" <?php if ($lang=="it") echo "selected" ?>>Talijanski</option>
                <option value="fr" <?php if ($lang=="fr") echo "selected" ?>>Francuski</option>
            </select>
        </label>
        <label>Sadržaj headera:<textarea name="header" placeholder="Sadržaj headera"><?php if (isset($header)) echo $header ?></textarea></label>

        <label>Sadržaj tijela:<textarea id="body" name="body" placeholder="Sadržaj tijela web stranice"><?php if (isset($body)) echo $body ?></textarea></label>

        <label>Sadržaj footera:<textarea name="footer" placeholder="Sadržaj footera"><?php if (isset($footer)) echo $footer ?></textarea></label>
        <input type="submit" name="submit" value="Generiraj"/>
    </form>
</main>
<?php  
if (isset($_POST['submit'])) {


    $output = "<!DOCTYPE html>\n";
    $output.= "<html lang=\"{$lang}\">\n";
    $output.= "<head>\n";
    $output.= "\t<meta charset=\"UTF-8\">\n";
    
    $output.="\t<title>$title</title>\n";

    $output.="</head>\n<body>\n<header>\n";
    
    $output.="\t<h1>$header</h1>\n</header>\n<main>\n";

   
    $output.="\t<p>$body</p>\n</main>\n<footer>\n";

    
    $output.="\t<p>$footer</p>\n</footer>\n";

    $output.="</body>\n</html>";
}
?>
<div class="right">
<pre class="prettyprint" contenteditable="true" spellcheck="false"><div id="selectable"><?php 
if (!isset($_POST['submit'])) {
    echo "Rezultate možete očekivati \novdje nakon što kliknete \nGeneriraj.";
}
else echo htmlspecialchars($output); 
?></div></pre>

<a href="javascript:selectText('selectable')" class="button" title="Označi sav generiran HTML kod" >Označi sve</a>

<button class="button" name="preview" type="submit" form="html-form" formaction="preview.php" formtarget="_blank" title="Pregledaj kako generirani kod izgleda u browseru">Pregled</button>

<button class="button" name="preview" id="quickPreview" type="submit" form="html-form" formaction="preview.php" formtarget="preview" title="Pregledaj kako generirani kod izgleda u browseru, klikni još jednom za zatvaranje">Brzi pregled</button>

<iframe src="preview.php" frameborder="0" id="preview" name="preview"></iframe>
</div>

<footer>Copyright &copy; 2014 Alen Duda</footer>
</body>
</html>